package py.una.entidad;
import java.util.ArrayList;
import java.util.List;

public class Persona {

	Long cedula;
	String nombre;
	String apellido;
	
	List<Vehiculo> vehiculos;
	
	public Persona(){
		vehiculos = new ArrayList<Vehiculo>();
	}

	public Persona(String pcedula, String pnombre, String papellido){
        try {
        	this.cedula = Long.parseLong(pcedula);
        }catch(Exception e1) {
        	
        }
		this.nombre = pnombre;
		this.apellido = papellido;
		
		vehiculos = new ArrayList<Vehiculo>();
	
	}
	
	public Long getCedula() {
		return cedula;
	}

	public void setCedula(Long cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public List<Vehiculo> getVehiculos() {
		return vehiculos;
	}

	public void setVehiculos(List<Vehiculo> a) {
		this.vehiculos = a;
	}
	
	public void addVehiculos(String vchapa, String vmarca) {
		Vehiculo nuevo = new Vehiculo(vchapa, vmarca);
		this.vehiculos.add(nuevo);
	}
}
