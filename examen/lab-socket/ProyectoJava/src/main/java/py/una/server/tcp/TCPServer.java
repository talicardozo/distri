package py.una.server.tcp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import py.una.entidad.Persona;
import py.una.entidad.Vehiculo;

import java.net.*;
import java.io.*;

public class TCPServer {
	public static ArrayList<Persona> lista = new ArrayList<Persona>();

    public static void main(String[] args) throws Exception {
    	
    	
    	
        int puertoServidor = 4444;
        int tiempo_procesamiento_miliseg = 2000;
		
		try{
			tiempo_procesamiento_miliseg = Integer.parseInt(args[0]);
		}catch(Exception e1){
			System.out.println("Se omite el argumento, tiempo de procesamiento " + tiempo_procesamiento_miliseg  + ". Ref: " + e1);
		}
		
		
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(puertoServidor);
        } catch (IOException e) {
            System.err.println("No se puede abrir el puerto: " +puertoServidor+ ".");
            System.exit(1);
        }
        System.out.println("Puerto abierto: "+puertoServidor+".");
        Socket clientSocket = null;
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            System.err.println("Fallo el accept().");
            System.exit(1);
        }

        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                clientSocket.getInputStream()));
        
        String inputLine, outputLine;
        outputLine = "";
        out.println("Bienvenido! Ingrese los datos separados por :");
        out.println("(Para agregar) opcion:cedula:nombre:apellido:chapa:marca |(Ej) - 1:598:Natalia:Cardozo:abc123:toyota");
        out.println("(Para consultar) opcion:cedula |(Ej) - 2:598");
        while ((inputLine = in.readLine()) != null) {
            //System.out.println("Seleccionada opción: " + inputLine);
            
            //out.println(inputLine);
        	String[] entrada  = inputLine.split(":",7);
            if (inputLine.equals("Bye")) {
                outputLine = "Bye";
                out.println("Bye");
                break;
                
            }else if (entrada[0]=="1") {
            	
            	Persona nuevo = new Persona(entrada[1], entrada[2], entrada[3]);
            	int i = existe(nuevo.getCedula());
            	if (i>=0) {
            		lista.get(i).addVehiculos(entrada[4], entrada[5]);
            	}
            	outputLine = "Usuario/a "+nuevo.getNombre()+" y vehiculo agregado";

            }else if (entrada[0]=="2"){
            	
            	List<Vehiculo> vusuario = new ArrayList<Vehiculo>();
            	Long nuevo=0L;
            	try {
                	nuevo = Long.parseLong(entrada[1]);
                }catch(Exception e1) {
                	
                }
            	outputLine = "El usuario";
            	int i = existe(nuevo);
            	if (i>=0) {
            		vusuario = lista.get(i).getVehiculos();
            		Iterator<Vehiculo> iter = vusuario.iterator();
            		outputLine = outputLine + " posee la siguiente Lista de vehiculos: " ;
                    while (iter.hasNext()) { 
                    	Vehiculo vehi = iter.next();
                    	outputLine = outputLine + " - |Chapa: " + vehi.getChapa() + ", Marca: " + vehi.getMarca(); 
                    }	
            	} else {
            		outputLine = outputLine + " no existe";
            	}
            }
            out.println(outputLine);
        }
        

        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }
        
    public static Integer existe (Long nuevo) {
    	Iterator<Persona> iter = lista.iterator();
    	while (iter.hasNext()) { 
    		Persona elemento = iter.next();
        	if(elemento.getCedula().equals(nuevo)){
        		return lista.indexOf(elemento);
        	}
        }
    	return -1;
    }
}
