package py.una.entidad;


public class Vehiculo {

	String chapa;
	String marca;
	
	public Vehiculo (String vchapa, String vmarca) {
		this.chapa = vchapa;
		this.marca = vmarca;
	}
	
	public String getChapa() {
		return chapa;
	}

	public void setChapa(String chappa) {
		this.chapa = chappa;
	}
	
	public String getMarca() {
		return marca;
	}

}